module.exports = function (type) {
  const prototype = {}
  const attributes = {}

  function addAttribute(attrName, attrType) {
    attributes[attrName] = { attrType }

    return this
  }

  const handler = {
    construct (target, args) {
      console.log('build it')
    },

    get (target, property, receiver) {
      switch (property) {
        case 'attribute':
          return addAttribute.bind(receiver)
          break
        default:
          throw new Error(`No property ${property}`)
      }
    }
  }

  return new Proxy(attributes, handler)
  
  //  attribute (attrName, attrType) {
  //    attributes[attrName] = { attrType }

  //    return this
  //  },

  //  build (params) {
  //    console.error('called')
  //    return new Proxy(obj, handler)
  //  }
  //}
}


//module.exports = function (type) {
//  const obj = {}
//  const attributes = {}
//
//  const handler = {
//    get (target, prop) {
//      console.error('Getting', target, prop)
//
//      if (!attributes.hasOwnPropety(prop)) {
//        throw new Error(`Undefined property ${prop}`)
//      }
//
//      return obj[prop]
//    },
//
//    set (target, prop, value) {
//      console.error('Setting', target, prop, value)
//
//      if (!attributes.hasOwnPropety(prop)) {
//        throw new Error(`Undefined property ${prop}`)
//      }
//
//      obj[prop] = value
//    }
//  }
//
//  return {
//    attribute (attrName, type) {
//      attributes[attrName] = { type }
//
//      return this
//    },
//
//    build (params) {
//      console.error('called')
//      return new Proxy(obj, handler)
//    }
//  }
//}
