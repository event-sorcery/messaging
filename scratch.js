// Have to define the getter and setter when the instance is actually made?

// A new instance should have the type-checked attributes
// An instance coming from build should have the type-checked attributes copied
// can I do a proxy that when new'd returns a non-proxy?

function constructMessage (type, attributes) {
  // this returns a proxy that does the attribute checks and gives an
  // error if you get a property that doesn't exit?
  const message = {
    type,
   
  }

  return {

  }
}

const M = function (type) {
  const attributes = {}

  function attribute (attrName, type) {
    attributes[attrName] = { type }

    return this
  }

  const handler = {
    construct (target, args) {
      console.log('newing it', target, args)
      return construct
    },

    get (target, attr) {
      if ('attribute' === attr) {
        return attribute
      }
    }
  }

  return new Proxy(foo, handler)
}

const Opened = M('Opened')
//  .attribute('accountId', String)

const opened = new Opened()
