const test = require('tape')

const Message = require('../../../lib/messaging/message')

test.skip('Defines attribute', t => {
  const Opened = Message()
    .attribute('accountId', String)

  const accountId = '123'
  const opened = Opened.build({ accountId })

  t.equals(accountId, opened.accountId)

  t.end()
})
