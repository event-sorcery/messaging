const Message = require('../../../lib/messaging/message')

module.exports = Message('Opened')
  .attribute('accountId', String)
