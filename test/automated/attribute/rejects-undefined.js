const test = require('tape')

const Opened = require('./opened-message')

test('Defines attribute', t => {
  const opened = Opened.build()

  t.throws(() => {
    opened.notAttr = 'nope'
  })

  t.end()
})
